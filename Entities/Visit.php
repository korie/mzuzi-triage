<?php
	
	
	namespace Modules\Triage\Entities;
	
	use App\Models\Facility\Visit as ParentVisit;
	
	class Visit extends ParentVisit
	{
		public function observations()
		{
			return $this->hasMany(Observation::class);
		}
		
	}