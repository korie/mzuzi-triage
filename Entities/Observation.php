<?php
	
	
	namespace Modules\Triage\Entities;
	
	
	use App\Models\Facility\Concept;
	use App\Medyq;
	
	class Observation extends Medyq
	{
		public function concept()
		{
			return $this->belongsTo(Concept::class);
		}
		
		public function fields()
		{
			return $this->hasMany(FieldObservation::class);
		}
	}