@extends('partials.facility.app')
@push('styles')
	<link rel="stylesheet" href="{{asset('libs/bootstrap-select/dist/css/bootstrap-select.min.css')}}">
	<style type="text/css">
		#vitalsTable tbody tr td:first-child {
			width: 50%;
		}
	</style>
@endpush
@push('content')
	<section class="content-header">
		<ol class="breadcrumb">
			<li>
				<a href="{{route('facility.dashboard', $subdomain)}}">
					<i class="fa fa-dashboard"></i> {{trans('facility.general.dashboard')}}
				</a>
			</li>
			<li>
				<a href="{{route('triage.queue')}}">
					<i class="fa fa-dashboard"></i> {{trans('triage::general.title')}}
				</a>
			</li>
			<li class="active">{{$patient->full_name}}</li>
		</ol>
		<div class="margin-bottom"></div>
	</section>
	<section class="content container-fluid">
		@include('facility.patients.patient-header')
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h4 class="box-title margin-top-5">
							<span class="text-bold">{{trans('facility.visits.number')}}</span>
							<span>{{$visit->number}}</span>
						</h4>
						<div class="clearfix"></div>
						<h4 class="box-title margin-top-5">
							<span class="text-bold">{{trans('facility.clinics.name_single')}}:</span>
							<span>{{$visit->clinic->name}}</span>
						</h4>
					</div>
					<div class="box-body">
						<form>
							<table id="captureVitalsTable" class="table no-border table-condensed">
								<tbody>
								<?php $concepts = $visit->clinic->concepts?>
								@foreach($concepts as $concept)
									<tr>
										<td class="text-right concept-field" data-concept="{{$concept->name}}"
										    data-uuid="{{$concept->uuid}}">
											<label>{{$concept->name}}</label>
										</td>
										@foreach($concept->fields as $field)
											<td class="text-left">
												@switch($field->type)
													@case('field')
													<input class="form-control" data-concept-name="{{$concept->name}}"
													       name="{{$field->name}}"
													       placeholder="{{$field->name}}"
													       type="text"/>
													@break
													@case('separator')
													<span class="text-bold"
													      data-concept-name="{{$concept->name}}"
													      data-type="separator">{{$field->name}}</span>
													@break
													@case('unit')
													<span class="text-bold"
													      data-concept-name="{{$concept->name}}"
													      data-type="unit">{{$field->name}}</span>
													@break
												@endswitch
											</td>
										@endforeach
									</tr>
								@endforeach
								</tbody>
							</table>
						</form>
					</div>
					<div class="box-footer text-right">
						<button id="saveButton" type="button" class="btn btn-success">
							{{trans('app.general.save')}}
						</button>
					</div>
				</div>
			</div>
		</div>
	</section>
@endpush
@push('footer_end')
	<script type="text/javascript" src="{{asset('libs/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
	<script type="text/javascript">
		$(function () {
			$("#saveButton").click(function (event) {
				let concepts = $(".concept-field");
				let data = [];
				
				concepts.each(function (index, element) {
					let conceptName = $(element).data('concept');
					let conceptUuid = $(element).data('uuid');
					let fields = [];
					$("[data-concept-name='" + conceptName + "']").each(function (index, element) {
						element = $(element);
						
						if (element.is("input")) {
							fields.push(
									{
										'text': element.val(),
										'type': 'text'
									}
							)
						} else {
							fields.push(
									{
										'text': element.html(),
										'type': element.data('type')
									}
							)
						}
					});
					
					data.push({
						'concept': conceptUuid,
						'fields': fields,
					})
					
				});
				
				request({
					url: '{{route('triage.capture', [$visit->uuid])}}',
					type: 'POST',
					data: {data: data},
					callback: function (response) {
						toastr.success(response[0]);
						window.location.replace("{{route('triage.queue')}}");
					},
				});
				
			});
			
			
		})
	</script>
@endpush