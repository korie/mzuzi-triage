@extends('partials.facility.app')
@push('styles')
	@include('partials.global.table-styles')
	<style type="text/css">
		
		#visitsTable tr td {
			text-align: center;
		}
		
		table.dataTable thead .sorting_asc:after {
			content: "";
		}
	
	</style>
@endpush
@push('content')
	<section class="content-header">
		<h1>
			{{trans('triage::general.title')}}
			<small>{{trans('triage::general.title_desc')}}</small>
		</h1>
		<ol class="breadcrumb">
			<li>
				<a href="{{route('facility.dashboard', [$subdomain])}}">
					<i class="fa fa-dashboard"></i> {{trans('app.general.dashboard')}}
				</a>
			</li>
			<li class="active">{{trans('triage::general.title')}}</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12">
										<table class="table table-striped" id="visitsTable">
											<thead>
											<tr>
												<th class="text-center">{{trans('facility.visits.number')}}</th>
												<th class="text-center">{{trans('facility.visits.patient_name')}}</th>
												<th class="text-center">{{trans('facility.visits.visit_type')}}</th>
												<th class="text-center">{{trans('facility.visits.emergency')}}</th>
												<th class="text-center">{{trans('app.general.actions')}}</th>
											</tr>
											</thead>
											<tbody>
											
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endpush
@push('footer_end')
	@include('partials.global.table-scripts')
	<script type="text/javascript">
		
		let visitsTable;
		
		$(function () {
			
			initTable()
			
		});
		
		function initTable() {
			
			let tableName = $("#visitsTable");
			
			visitsTable = $(tableName).DataTable({
				responsive: false,
				serverSide: true,
				lengthMenu: [[100, 250, 500, 1000, 2500, 10000 /*, -1*/], [100, 250, 500, 1000, 2500, 10000 /*, 'All'*/]],
				cache: false,
				stateSave: false,
				columnDefs: [
					{
						bSortable: false,
						aTargets: [0, 1, 2, 3, 4, -1]
					},
				],
				columns: [
					{data: "number"},
					{data: "full_name"},
					{data: "title"},
					{data: "is_emergency"},
					{data: "actions"},
				],
				language: dataTablesTranslations,
				ajax: {
					url: '{{route('triage.queue.rest')}}',
					beforeSend: function (xhr) {
						showLoadingAnimation();
					}
					, complete: function (xhr) {
						hideLoadingAnimation();
					},
					error: function (xhr, error, code) {
						let message = "";
						$.each(xhr.responseJSON.messages, function (index, str) {
							message = message.concat(str).concat('\n');
						});
						showErrorAlert(message);
					}
				},
				drawCallback: function (settings) {
					initDefaultElements();
				}
			});
		}
	
	</script>
@endpush