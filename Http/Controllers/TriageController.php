<?php
	
	namespace Modules\Triage\Http\Controllers;
	
	use App\Facility;
	use App\Models\Facility\Concept;
	use Illuminate\Http\Request;
	use Illuminate\Http\Response;
	use Illuminate\Routing\Controller;
	use Modules\Triage\Entities\FieldObservation;
	use Modules\Triage\Entities\Observation;
	use Modules\Triage\Entities\Visit;
	use Modules\Triage\Http\Requests\StoreObservationRequest;
	
	class TriageController extends Controller
	{
		/**
		 * Display a listing of the resource.
		 * @return Response
		 */
		public function index()
		{
			return view('triage::index');
		}
		
		public function indexRest(Request $request)
		{
			$facility = get_current_facility($request->url());
			
			$builder = Visit::select(
					'uuid', 'number', 'created_at AS visit_date', 'patient_id', 'visit_type_id', 'visit_level',
					'is_emergency', 'ended_at'
			)/*->whereDate('created_at', Carbon::today())*/
			;
			
			$search = request('search', '');
			$start = (int)request('start', '0');
			$length = (int)request('length', 100);
			
			$builder->whereNull('ended_at');
			
			if (!empty($search) && !empty($search['value'])) {
				$builder->whereHas('patient', function ($query) use ($search) {
					$query->search($search['value']);
				});
			}
			
			$builder->orderBy('is_emergency', 'desc');
			$builder->orderBy('id', 'asc');
			
			if ($length != -1) {
				$builder->offset($start);
				$builder->limit($length);
			}
			
			$builder->doesntHave('observations');
			
			$visits = $builder->get();
			
			$recordsFiltered = $visits->count();
			
			$recordsTotal = Visit::doesntHave('observations')->count();
			
			$visits->load([
					'patient' => function ($query) {
						$query->select('id', 'uuid', 'first_name', 'middle_name', 'last_name', 'dob');
					},
					'type' => function ($query) {
						$query->select('id', 'uuid', 'title');
					},
					'level' => function ($query) {
						$query->select('order', 'department_id');
						$query->with(['department' => function ($query) {
							$query->select('id', 'uuid', 'name');
						}]);
					}
			]);
			
			$visits = $visits->map(function ($visit) use ($facility) {
				
				return [
						'number' => $visit->number,
						'full_name' => '<a href="'
								. route('facility.patients.show', [$facility->subdomain, $visit->patient->uuid])
								. '">' . $visit->patient->full_name . '</a>',
						'title' => $visit->type->title,
						'is_emergency' => $visit->is_emergency,
						'actions' => '<a href="' . route('triage.capture', [$visit->uuid])
								. '" class="btn btn-success btn-sm">' .
								trans('triage::general.capture_vitals')
								. '</a> ',
				];
			});
			
			return [
					'draw' => request('draw', 0),
					'recordsTotal' => $recordsTotal,
					'recordsFiltered' => $recordsFiltered,
					'data' => $visits
			];
		}
		
		/**
		 * Show the form for creating a new resource.
		 * @param Visit $visit
		 * @return Response
		 */
		public function captureVitals(Visit $visit)
		{
			$visit->load(['patient', 'clinic' => function ($query) {
				$query->active();
				$query->with(['concepts' => function ($query) {
					$query->active();
					$query->orderBy('id', 'asc');
					$query->with(['fields' => function ($query) {
						$query->orderBy('id', 'asc');
					}]);
				}]);
			}]);
			return view('triage::capture-vitals')->with(['visit' => $visit, 'patient' => $visit->patient]);
		}
		
		/**
		 * Store a newly created resource in storage.
		 * @param StoreObservationRequest $request
		 * @param Facility $facility
		 * @param Visit $visit
		 * @return array
		 */
		public function store(StoreObservationRequest $request, Facility $facility, Visit $visit)
		{
			$data = request('data');
			
			$visitId = $visit->id;
			
			foreach ($data as $row) {
				$concept = Concept::where('uuid', $row['concept'])->first();
				$observation = new Observation();
				$observation->visit_id = $visitId;
				$observation->concept_id = $concept->id;
				$observation->save();
				foreach ($row['fields'] as $conceptField) {
					$field = new FieldObservation();
					$field->observation_id = $observation->id;
					$field->type = $conceptField['type'];
					$field->text = $conceptField['text'] . '';
					$field->save();
				}
			}
			
			$visit->visit_level = 2;
			$visit->save();
			
			return [trans('app.general.saved')];
			
		}
		
		/**
		 * Show the specified resource.
		 * @param int $id
		 * @return Response
		 */
		public function show($id)
		{
			return view('triage::show');
		}
		
		/**
		 * Show the form for editing the specified resource.
		 * @param int $id
		 * @return Response
		 */
		public function edit($id)
		{
			return view('triage::edit');
		}
		
		/**
		 * Update the specified resource in storage.
		 * @param Request $request
		 * @param int $id
		 * @return Response
		 */
		public function update(Request $request, $id)
		{
			//
		}
		
		/**
		 * Remove the specified resource from storage.
		 * @param int $id
		 * @return Response
		 */
		public function destroy($id)
		{
			//
		}
	}
