<?php
	
	namespace Modules\Triage\Providers;
	
	use Illuminate\Database\Eloquent\Factory;
	use Illuminate\Support\ServiceProvider;
	
	class TriageServiceProvider extends ServiceProvider
	{
		/**
		 * Indicates if loading of the provider is deferred.
		 *
		 * @var bool
		 */
		protected $defer = false;
		
		/**
		 * Boot the application events.
		 *
		 * @return void
		 */
		public function boot()
		{
			$this->registerTranslations();
			$this->registerConfig();
			$this->registerViews();
			$this->registerFactories();
			//$this->overrideMenu();
			$this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
		}
		
		private function overrideMenu()
		{
			//To hide menu
			//override_menu('sample-menus-module', false, GlobalHelpers::FACILITY_SIDE_NAV_MENU);
			
			$menu = [
					[
							'url' => url('/triage'),
							'title' => trans('triage::general.title'),
							'icon' => 'fa fa-list',
							'type' => 'link',//optional
							'order' => 4,//optional
					],
					[
							'url' => url('/triage/concepts'),
							'title' => trans('triage::concepts.title'),
							'icon' => 'fa fa-list',
							'type' => 'link',//optional
							'order' => 10,//optional
					],
			];
			
			override_menu('triage', $menu);
		}
		
		/**
		 * Register the service provider.
		 *
		 * @return void
		 */
		public function register()
		{
			$this->app->register(RouteServiceProvider::class);
		}
		
		/**
		 * Register config.
		 *
		 * @return void
		 */
		protected function registerConfig()
		{
			$this->publishes([
					__DIR__ . '/../Config/config.php' => config_path('triage.php'),
			], 'config');
			$this->mergeConfigFrom(
					__DIR__ . '/../Config/config.php', 'triage'
			);
		}
		
		/**
		 * Register views.
		 *
		 * @return void
		 */
		public function registerViews()
		{
			$viewPath = resource_path('views/modules/triage');
			
			$sourcePath = __DIR__ . '/../Resources/views';
			
			$this->publishes([
					$sourcePath => $viewPath
			], 'views');
			
			$this->loadViewsFrom(array_merge(array_map(function ($path) {
				return $path . '/modules/triage';
			}, \Config::get('view.paths')), [$sourcePath]), 'triage');
		}
		
		/**
		 * Register translations.
		 *
		 * @return void
		 */
		public function registerTranslations()
		{
			$langPath = resource_path('lang/modules/triage');
			
			if (is_dir($langPath)) {
				$this->loadTranslationsFrom($langPath, 'triage');
			} else {
				$this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'triage');
			}
		}
		
		/**
		 * Register an additional directory of factories.
		 *
		 * @return void
		 */
		public function registerFactories()
		{
			if (!app()->environment('production')) {
				app(Factory::class)->load(__DIR__ . '/../Database/factories');
			}
		}
		
		/**
		 * Get the services provided by the provider.
		 *
		 * @return array
		 */
		public function provides()
		{
			return [];
		}
	}
