<?php
	
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;
	
	class CreateFieldObservationTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::create('field_observations', function (Blueprint $table) {
				$table->increments('id');
				$table->uuid('uuid')->unique();
				$table->unsignedInteger('observation_id');
				$table->string('text');
				$table->string('type');
				$table->unsignedInteger('created_by')->nullable();
				$table->unsignedInteger('updated_by')->nullable();
				$table->unsignedInteger('deleted_by')->nullable();
				$table->unsignedInteger('restored_by')->nullable();
				$table->timestamp('restored_at')->nullable();
				$table->timestamps();
				$table->softDeletes();
				
				$parentSchema = env('DB_SCHEMA', 'public');
				
				$table->foreign('observation_id')->references('id')->on('observations')->onDelete('cascade');
				$table->foreign('created_by')->references('id')->on($parentSchema . '.users')->onDelete('cascade');
				$table->foreign('updated_by')->references('id')->on($parentSchema . '.users')->onDelete('cascade');
				$table->foreign('deleted_by')->references('id')->on($parentSchema . '.users')->onDelete('cascade');
				$table->foreign('restored_by')->references('id')->on($parentSchema . '.users')->onDelete('cascade');
				
				$table->index(['uuid']);
			});
		}
		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('field_observations');
		}
	}
