<?php
	
	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/
	
	Route::prefix('triage')->group(function () {
		Route::get('/', 'TriageController@index')->name('triage.queue');
		Route::get('/rest', 'TriageController@indexRest')->name('triage.queue.rest');
		Route::get('/{visit}/capture/', 'TriageController@captureVitals')->name('triage.capture');
		Route::post('/{visit}/capture/', 'TriageController@store');
	});
